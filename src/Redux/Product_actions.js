import { FETCH_PRODUCT_REQ,FETCH_PRODUCT_SUCCESS,FETCH_PRODUCT_ERROR } from "./Actions_constants"
import axios from "axios"


const fetchProductReaquest=()=>{
    return{
        type:FETCH_PRODUCT_REQ, 
    }
}

const fetchProductSuccess=(products)=>{
    console.log(products)

    return{
        type:FETCH_PRODUCT_SUCCESS,
        payload:products
    }
}

const fetchProductError=(error)=>{
    return{
        type:FETCH_PRODUCT_ERROR,
        payload:error
    }
}

const addtocart=(product)=>{
    
}
//THUNK FUNCTION
export function fetchPRODUCTS(){
    return function (dispatch)  {
        dispatch(fetchProductReaquest())
        axios
        .get('https://fakestoreapi.com/products')
        .then(response=>{
            const products=response.data
            console.log("response",products)
            dispatch(fetchProductSuccess(products))
        })
        .catch(error=>{
            const errmsg=error.message
            dispatch(fetchProductError(errmsg))
        })
    }
}

export {fetchProductError,fetchProductReaquest,fetchProductSuccess}