const { combineReducers } = require("redux");
const { default: productReducer } = require("./Product_Reducer");

const rootreducer=combineReducers({
    product:productReducer
})
export default rootreducer