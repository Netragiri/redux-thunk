import { FETCH_PRODUCT_REQ,FETCH_PRODUCT_SUCCESS,FETCH_PRODUCT_ERROR } from "./Actions_constants"

const initialState={
    products:[],
    error:'',
    loading:false
}

const productReducer=(state=initialState,action)=>{
    switch(action.type){
        case FETCH_PRODUCT_REQ:
            return{
                ...state,
                loading:true
        }
        case FETCH_PRODUCT_SUCCESS:
            return{
                loading:false,
                products:action.payload,
                error:''
        }
        case FETCH_PRODUCT_ERROR:
            return{
                loading:false,
                products:[],
                error:action.payload
            }
        default : return state
    }
}

export default productReducer;