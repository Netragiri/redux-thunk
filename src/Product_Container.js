import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchPRODUCTS } from './Redux/Product_actions'

function Product_Container({fetchproduct,productData}) {


  useEffect(()=>{
    fetchproduct()
  },[])


  const addtocart=(product)=>{
    console.log(product)
  }


  return productData.loading ?(<h2>loading</h2>) : productData.error ? (<h2>{productData.error}</h2>)
  :(
    <>
      <h2>Product List</h2>
      <div className='container'>
      <div className='row'>
        {
          productData.products.map((product,index) => 
          
            <div className="card col-md-4" key={index}>
              <div>
                    <img className="card-img-top" src={product.image} alt="Card image cap" style={{height:'200px', width:'200px'}} />
              </div>
            <div className="card-body">
              <h5 className="card-title">{product.title}</h5>
              <p className="card-text">Price :{product.price}</p>
              <a href="#" className="btn btn-primary" onClick={()=>{addtocart(product)}}  >Add to cart</a>
            </div>
          </div>
          
          )}
      </div>
      </div>
    </>
  )
}








const mapStateToProps=state=>{
  return{
    productData:state.product
  }
}
const mapDispatchToProps=dispatch=>{
  return{
    fetchproduct:()=>dispatch(fetchPRODUCTS())
    
  }
}
export default
 connect(mapStateToProps,mapDispatchToProps) 
 (Product_Container)
