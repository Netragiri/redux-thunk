import React from 'react'
import { useNavigate } from 'react-router-dom'
import Cart from '../Cart/Cart'

function Header() {
    const navigate=useNavigate()
  return (
    <div>
       <nav className="navbar navbar-expand-lg navbar-light bg-light ">
            <a className="navbar-brand" href="#">Navbar</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                <button className='btn btn-link'>Cart</button>
                <Cart />
            </div>
        </div>
        </nav>
    </div>
  )
}

export default Header
