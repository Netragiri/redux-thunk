import React from 'react'
import Product_Container from '../Product_Container'
import Header from './Header'

function Home() {
  return (
    <div>
      <Header />
      <Product_Container />
    </div>
  )
}

export default Home
