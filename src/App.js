import logo from './logo.svg';
import './App.css';
import { Provider } from 'react-redux';
import store from './Redux/Store';
import Home from './components/Home';


function App() {
  return (
    <Provider store={store}>
    <div className="App">
      <Home />
    </div>
    </Provider>
  );
}

export default App;
